##########################################################################################################################
# File automatically-generated by tool: [projectgenerator] version: [3.16.0] date: [Fri Mar 25 12:34:34 CET 2022]
##########################################################################################################################

# ------------------------------------------------
# Generic Makefile (based on gcc)
#
# ChangeLog :
#	2017-02-10 - Several enhancements + project update mode
#   2015-07-22 - first version
# ------------------------------------------------

######################################
# target
######################################
TARGET = bts


######################################
# building variables
######################################
# debug build?
DEBUG = 1
# optimization
OPT = -Og


#######################################
# paths
#######################################
# Build path
BUILD_DIR = build

######################################
# source
######################################
# C sources
C_SOURCES =  \
application/main.c \
application/stm32l4xx_it.c \
application/stm32l4xx_hal_msp.c \
application/bsp_init.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c \
hal/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c \
application/system_stm32l4xx.c  

# ASM sources
ASM_SOURCES =  \
startup/startup_stm32l476xx.s


#######################################
# binaries
#######################################
BINPATH = ./toolchain/bin
PREFIX = arm-none-eabi-
CC = $(BINPATH)/$(PREFIX)gcc
AS = $(BINPATH)/$(PREFIX)gcc -x assembler-with-cpp
CP = $(BINPATH)/$(PREFIX)objcopy
SZ = $(BINPATH)/$(PREFIX)size
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S
 
#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=cortex-m4

# fpu
FPU = -mfpu=fpv4-sp-d16

# float-abi
FLOAT-ABI = -mfloat-abi=hard

# mcu
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

# macros for gcc
# AS defines
AS_DEFS = 

# C defines
C_DEFS =  \
-DUSE_HAL_DRIVER \
-DSTM32L476xx


# AS includes
AS_INCLUDES = 

# C includes
C_INCLUDES =  \
-Iapplication \
-Ihal/STM32L4xx_HAL_Driver/Inc \
-Ihal/STM32L4xx_HAL_Driver/Inc/Legacy \
-Ihal/CMSIS/Device/ST/STM32L4xx/Include \
-Ihal/CMSIS/Include


# compile gcc flags
ASFLAGS = $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections

CFLAGS += $(MCU) $(C_DEFS) $(C_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections

ifeq ($(DEBUG), 1)
CFLAGS += -g -gdwarf-2
endif


# Generate dependency information
CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)"


#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT = startup/STM32L476RGTx_FLASH.ld

# libraries
LIBS = -lc -lm -lnosys 
LIBDIR = 
LDFLAGS = $(MCU) -specs=nano.specs -T$(LDSCRIPT) $(LIBDIR) $(LIBS) -Wl,-Map=$(BUILD_DIR)/$(TARGET).map,--cref -Wl,--gc-sections

# default action: build all
all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin


#######################################
# build the application
#######################################
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))
# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR) 
	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)
	$(AS) -c $(CFLAGS) $< -o $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@
	$(SZ) $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(HEX) $< $@
	
$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(BIN) $< $@	
	
$(BUILD_DIR):
	mkdir $@		

#######################################
# clean up
#######################################
clean:
	-rm -fR $(BUILD_DIR)
  
#######################################
# dependencies
#######################################
-include $(wildcard $(BUILD_DIR)/*.d)

# *** EOF ***